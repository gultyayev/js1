function showLargestSequence(str1, str2) {
    /**
     * Если не хватает аргументов - ошибка
     */
    if (str1 == undefined || str2 == undefined) {
        throw new Error('Not enough parameters passed');
    }
    
    /**
     * Отсекаем вариант когда первая строка пуста
     */
    if (str1.length === 0) return '';

    /**
     * Если передано значение отличное от строкового выбрасываем ошибку
     */
    if (typeof str1 !== 'string' || typeof str2 !== 'string') {
        const err = new Error();
        err.name = 'Type violation';
        err.message = `Strings were expected. "${typeof str1}" and "${typeof str2}" got instead`;
        err.stack = (new Error).stack;

        throw err;
    }

    /**
     * Здесь будем хранить все найденные соответствия букв
     * в порядке первой строки
     * 
     * Т.е. если мы имеем на входе "ABBA" и "ABDASDB"
     * мы получим такой массив
     * 0: [0, 3]
     * 1: [1, 6]
     * 2: [1, 6]
     * 3: [0, 3]
     */
	let result = [];
    
    /**
     * Проходимся по каждой строке
     */
	for (let i = 0; i < str1.length; i++) {
		/**
         * Все вхождения текущего символа
         */
		let occurencies = [];
		
		/**
         * Функция поиска вхождения символа во второй строке.
         * Все найденные индексы попадают в `occurencies`
         *
         * @param {string} str Строка в которой нужно искать
         * @param {string} char Символ поиск которого осуществляется
         * @param {number} [startIndex=0] Индекс с которого начинаем поиск
         */
        function find(str, char, startIndex = 0) {
			/**
             * Индекс найденного символа
             */
			let index = str.indexOf(char, startIndex);
            
            /**
             * Если был найден символ то начинаем новый поиск с подстроки
             * начинающейся со следующего символа
             */
			if (index !== -1) {
				occurencies.push(index);
				find(str, char, ++index);
			}
        }
        
        /**
         * Делаем инициализирующий вызов
         */
		find(str2, str1[i]);
        
        /**
         * Записываем найденные индесы в массив под соответствующим
         * текущей строке индексом
         */
		result[i] = occurencies.slice();
    }

	function reduce(array) {
        return array.reduce((acc, val, i, arr) => {
            /**
             * Если мы не на первом элементе массива, то смотрим,
             * чтобы текущее значение было больше предыдущего сохраненного
             */
            if (arr[i - 1]) {
                const filtered = val.filter(sub => sub > acc[acc.length - 1])
                let value = filtered.length > 1 ? [filtered[0]] : filtered;
                return [...acc, ...value];
            } else {
                return [Math.min(...val)];
            }
        }, 0)
    }

    /**
     * Для учитывания разных комбинаций проходимся по каждому массиву
     * постепенно уменьшая его на 1
     */
    const possibilities = result.map((res, i, arr) => reduce(arr.slice(i)));

    /**
     * Сортируем по убывания полученный массив и берем максимальное
     * значение
     */
    const max = possibilities.sort((a, b) => {
        if (a.length < b.length) {
            return 1;
        } else if (a.length > b.length) {
            return -1;
        } else {
            return 0;
        }
    })[0];

    /**
     * Возвращаем собранную строку из символов второй строки
     */
	return max.map(n => str2[n]).join('');
}
