describe('Google test', () => {

    it('should exist', () => {
        expect(showLargestSequence).toBeTruthy();
    })

    it('"ABAZDC" , "BACBAD" => "ABAD"', () => {
        expect(showLargestSequence('ABAZDC', 'BACBAD')).toBe('ABAD');
    })

    it('"AGGTAB", "GXTXAYB" => "GTAB"', () => {
        expect(showLargestSequence('AGGTAB', 'GXTXAYB')).toBe('GTAB');
    })

    it('"aaaa", "aa" => "aa"', () => {
        expect(showLargestSequence('aaaa', 'aa')).toBe('aa');
    })

    it('"", "..." => ""', () => {
        expect(showLargestSequence('', 'aa')).toBe('');
    })

    /**
     * Чтобы работало с "выбросом ошибок" нужно обернуть вызов функции
     */
    it('1, "AVASD" => Error', () => {
        expect(() => {
            showLargestSequence(1, 'AVASD');
        }).toThrowError();
    });

    it('undefined, "AASDXA" => Error', () => {
        expect(() => {
            showLargestSequence(undefined, 'AASDXA');
        }).toThrowError();
    });

    it('"QPEO", undefined => Error', () => {
        expect(() => {
            showLargestSequence('QPEO', undefined);
        }).toThrowError();
    });

    it('"QWERASGAS", "QPEWFAD" => ""', () => {
        expect(showLargestSequence('QWERASGAS', 'QPEWFAD')).toBe('QWA');
    });

    it('"CRABACO", "ABBACE" => "ABAC"', () => {
        expect(showLargestSequence('CRABACO', 'ABBACE')).toBe('ABAC')
    });

});